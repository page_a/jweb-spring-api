package eu.epitech.jweb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by alexa on 17/12/2015.
 */

/*
 * Entity Review. Note the @ManyToOne for foreign key. See Review repository.
 */

@Entity
@Table(name = "reviews",
        indexes = {@Index(name = "i_author_id", columnList = "author_id", unique = false)}
)
public class Review
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String comment;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Account author;

    @NotNull
    private Date date;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "product_id")
    private Product product;

    public Review(String comment, Product product, Account author, Date date)
    {
        this.comment = comment;
        this.product = product;
        this.author = author;
        this.date = date;
    }

    public Product getProduct()
    {
        return product;
    }

    public Review() {} // Needed for delete

    public Account getAuthor()
    {
        return author;
    }

    public Date getDate()
    {
        return date;
    }

    public Long getId()
    {
        return id;
    }

    public String getComment()
    {
        return comment;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setAuthor(Account author)
    {
        this.author = author;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setProduct(Product product)
    {
        this.product = product;
    }
}
