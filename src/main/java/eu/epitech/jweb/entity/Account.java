package eu.epitech.jweb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by alexa on 14/12/2015.
 */

/*
 * Entity for account. @NotNull using for validation exception
 */

@Entity
@Table(name = "users",
       indexes = {@Index(name = "i_user_email", columnList = "email", unique = true),
               @Index(name = "i_fist_name", columnList = "firstName", unique = false)})
public class Account
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = "boolean default true", nullable = false)
    private boolean newsletter;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String password;

    @NotNull
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    private String role;

    public Account() {}

    public Account(long id)
    {
        this.id = id;
    }

    public Account(String firstName, String lastName, String password, String email, String role,
                    boolean newsletter)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.role = role;
        this.newsletter = newsletter;
    }

    public Account(Long id)
    {
        this.id = id;
    }

    public boolean isNewsletter()
    {
        return newsletter;
    }

    public Long getId()
    {
        return id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getPassword()
    {
        return password;
    }

    public String getEmail()
    {
        return email;
    }

    public String getRole()
    {
        return role;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public void setNewsletter(boolean newsletter)
    {
        this.newsletter = newsletter;
    }
}
