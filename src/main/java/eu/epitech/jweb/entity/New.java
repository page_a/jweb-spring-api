package eu.epitech.jweb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by alexa on 20/12/2015.
 */

/*
 * Entity News.
 */

@Entity
@Table(name = "news")
public class New
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long NewId;

    @NotNull
    private String content;

    @NotNull
    private Long authorNewsId;

    public New(String content, Long authorId)
    {
        this.content = content;
        this.authorNewsId = authorId;
    }

    public New() {} // Needed for delete request

    public Long getNewId()
    {
        return NewId;
    }

    public String getContent()
    {
        return content;
    }

    public Long getAuthorId()
    {
        return authorNewsId;
    }

    public void setNewId(Long newId)
    {
        NewId = newId;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public void setAuthorId(Long authorId)
    {
        this.authorNewsId = authorId;
    }
}
