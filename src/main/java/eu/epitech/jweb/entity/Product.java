package eu.epitech.jweb.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by alexa on 18/12/2015.
 */

/*
 * Entity product.
 */

@Entity
@Table(name = "products")
public class Product
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long productId;

    @NotNull
    private String productName;

    @NotNull
    private String description;

    @NotNull
    private Float price;

    public Product(String productName, String description, Float price)
    {
        this.productName = productName;
        this.description = description;
        this.price = price;
    }

    public Product(Long productId)
    {
        this.productId = productId;
    }

    public Product() {}

    public float getPrice()
    {
        return price;
    }

    public Long getProductId()
    {
        return productId;
    }

    public String getProductName()
    {
        return productName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }
}
