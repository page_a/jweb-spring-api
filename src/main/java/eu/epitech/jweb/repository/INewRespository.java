package eu.epitech.jweb.repository;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.entity.New;
import eu.epitech.jweb.entity.Product;
import eu.epitech.jweb.entity.Review;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by alexa on 20/12/2015.
 */
/*
 * News repository for transaction.
 */

public interface INewRespository extends CrudRepository<New, Long>
{
    List<New> findAll();
}
