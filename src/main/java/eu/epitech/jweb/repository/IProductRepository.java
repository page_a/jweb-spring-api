package eu.epitech.jweb.repository;

import eu.epitech.jweb.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by alexa on 18/12/2015.
 */

/*
 * Repository for Product transaction.
 */
public interface IProductRepository extends CrudRepository<Product, Long>
{
    List<Product> findAll();
}
