package eu.epitech.jweb.repository;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.entity.Product;
import eu.epitech.jweb.entity.Review;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by alexa on 17/12/2015.
 */
/*
 * Review repository for transaction. Note here the '_'
 * The method here return all the Review linked to a product Id (=foreign key)
 */
@Transactional
public interface IReviewRepository extends CrudRepository<Review, Long>
{
    List<Review> findByProduct_ProductId(Long productId);
}
