package eu.epitech.jweb.repository;

import eu.epitech.jweb.entity.Account;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by alexa on 14/12/2015.
 */

/*
 * Repository for Account transaction. It's here where sql request are done. (Spring parse the method name)
 */

@Transactional
public interface IAccountRepository extends CrudRepository<Account, Long>
{
    Account findByEmail(String email);
    Account findFirstByEmail(String email);
    List<Account> findAll();
}