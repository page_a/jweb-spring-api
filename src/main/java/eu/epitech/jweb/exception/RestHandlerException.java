package eu.epitech.jweb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexa on 20/12/2015.
 */

/*
 * Class controller exception handler. It handle ConstraintViolationException, NullPointerException.
 * which are the most important exception.
 */
@ControllerAdvice
public class RestHandlerException
{

    private static final String STATUS = "status";
    private static final String MESSAGE = "message";
    private static final String PATH = "path";
    private static final String CONSTRAINT_VALIDATION_ERROR = "Constraint violation error - probably missing parameter or null parameter";
    private static final String NULL_POINTER_VALIDATION_ERROR = "Null pointer error - probably missing parameter";


    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public Map<String, String> handleConstraintViolationException(ConstraintViolationException ex, HttpServletResponse response,
                                            HttpServletRequest request){
        Map<String,String> errorMap = new HashMap<String,String>();

        errorMap.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.toString());
        errorMap.put(MESSAGE, CONSTRAINT_VALIDATION_ERROR );
        errorMap.put(PATH, request.getRequestURI());

        response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        return errorMap;
    }

    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public Map<String, String> handleNullPointerException(NullPointerException ex, HttpServletResponse response
            , HttpServletRequest request)
    {
        Map<String, String> error = new HashMap<String, String>();

        error.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.toString());
        error.put(MESSAGE, NULL_POINTER_VALIDATION_ERROR);
        error.put(PATH, request.getRequestURI());

        response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        return error;
    }

    // MAKE "org.springframework.web.HttpRequestMethodNotSupportedException"

}
