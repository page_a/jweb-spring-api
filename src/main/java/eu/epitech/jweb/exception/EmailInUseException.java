package eu.epitech.jweb.exception;

import org.hibernate.validator.constraints.Email;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by alexa on 20/12/2015.
 */
/*
 * Custom exception when email already in use.
 */

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "email already in use")
public final class EmailInUseException extends RuntimeException {
}