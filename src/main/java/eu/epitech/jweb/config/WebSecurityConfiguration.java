package eu.epitech.jweb.config;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by alexa on 16/12/2015.
 */

/*
 * Configuration for the basic auth. Here, I'm using the USER object of spring for managing the permission
 */
@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter
{

    @Autowired
    IAccountRepository IAccountRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    UserDetailsService userDetailsService()
    {
        return new UserDetailsService()
        {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
            {
                Account account = IAccountRepository.findByEmail(email);
                if (account != null)
                {
                    return new User(account.getEmail(), account.getPassword(), true, true, true, true,
                            AuthorityUtils.createAuthorityList(account.getRole()));
                } else
                {
                    throw new UsernameNotFoundException("could not find the user '"
                            + email + "'");
                }
            }

        };
    }
}
