package eu.epitech.jweb.controllers;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by alexa on 17/12/2015.
 */

/*
 * Controller for /auth road.
 * RestController allowing to authenticate visitor.
 * It is not securised as every visitor came ask to be authenticate.
 *
 * Road :
 *          /auth -> GET -> login()
 *
 */

@RestController
@RequestMapping("/auth")
public class AuthController
{
    @Autowired
    private IAccountRepository IAccountRepository;

    private static final String ACCOUNT = "account";
    private static final String STATUS = "status";
    private static final String MESSAGE = "message";
    private static final String UNAUTHORIZED = "unauthorized";


    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> login(String email, String password)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();
        Account account = IAccountRepository.findByEmail(email);

        if (account != null)
        {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (encoder.matches(password, account.getPassword()))
                response.put(ACCOUNT, account);
            else
                response.put(STATUS, Unauthorized("401"));
        }
        else
            response.put(STATUS, Unauthorized("403"));
        return response;
    }

    private static Map<String, String> Unauthorized(String statusCode)
    {
        Map<String, String> message = new LinkedHashMap<String, String>();
        message.put(MESSAGE, UNAUTHORIZED);
        message.put(STATUS, statusCode);
        return message;
    }
}
