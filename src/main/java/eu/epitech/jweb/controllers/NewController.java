package eu.epitech.jweb.controllers;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.entity.New;
import eu.epitech.jweb.entity.Review;
import eu.epitech.jweb.repository.IAccountRepository;
import eu.epitech.jweb.repository.INewRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexa on 20/12/2015.
 */

/*
 * Controller for /news road.
 * RestController allowing to get manage the news. creating, editing, deleting, retreving one.
 * It is securised, only admin can request with road. (@Secured)
 *
 * Road :
 *          /news -> GET -> getAuthorNews(Long id)
 *          /news/all -> GET -> List<New> listingAllNews()
 *          /news/ -> POST -> createNews(String content, Long authorId)
 *          /news/ -> PUT -> UpdateNews(String content, Long id)
 *          /news/ -> DELETE -> deleteNews(Long id)
 *
 */

@RestController
@RequestMapping(value = "/news")
@Secured({"ROLE_ADMIN"})
public class NewController
{
    private static final String NEW = "new";
    private static final String DELETE_SUCCESS = "New deleted successfully";
    private static final String UPDATE_SUCCESS = "New updated successfully";
    private static final String MESSAGE = "message";

    @Autowired
    private INewRespository newRespository;

    @Autowired
    private IAccountRepository accountRepository;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getAuthorNews(Long id)
    {
        Account a = accountRepository.findOne(id);
        Map<String, String> result = new HashMap<String, String>();
        result.put("AUTHOR", a.getFirstName() + " " + a.getLastName());
        return result;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    @ResponseBody
    public List<New> listingAllNews()
    {
        List<New> r = newRespository.findAll();
        return r;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createNews(String content, Long authorId)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();

        New nw = new New(content, authorId);

        response.put(NEW, newRespository.save(nw));
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> deleteNews(Long id)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();

        newRespository.delete(id);

        response.put(MESSAGE, DELETE_SUCCESS);
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> UpdateNews(String content, Long id)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();

        New nw = newRespository.findOne(id);

        nw.setContent(content);

        response.put(MESSAGE, UPDATE_SUCCESS);

        return response;
    }
}
