package eu.epitech.jweb.controllers;

import eu.epitech.jweb.entity.Product;
import eu.epitech.jweb.entity.Review;
import eu.epitech.jweb.repository.IProductRepository;
import eu.epitech.jweb.repository.IReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by alexa on 18/12/2015.
 */

/*
 * Controller for /product road.
 * RestController allowing to get product detail.
 * It is securised, only admin can request with road. (@Secured)
 *
 * Road :
 *          /product/all -> GET -> List<Map<String, Object> > getAllProduct()
 *          /product/{id} -> GET -> getProduct(Long id)
 *          /product/ -> POST -> addProduct(String description, String productName, Float price)
 *
 */

@RestController
@RequestMapping("/product")
public class ProductController
{

    private static final String CREATE_SUCCESS = "Product created successfully";
    private static final String PRODUCT = "product";
    private static final String REVIEW = "review";
    private static final String NO_COMMENT = "No comment yet";

    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IReviewRepository reviewRepository;

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    @ResponseBody
    public List<Map<String, Object> > getAllProduct()
    {

        List<Product> l = productRepository.findAll();
        List< Map<String, Object> > result = new LinkedList<>();

        Iterable<Product> it = l;
        for (Product p : it)
        {
            Long id = p.getProductId();
            List<Review> reviews = reviewRepository.findByProduct_ProductId(id);
            Map<String, Object> response = new LinkedHashMap<String, Object>();
            response.put(PRODUCT, p);
            if (reviews.size() == 0)
                response.put(REVIEW, NO_COMMENT);
            else
                response.put(REVIEW, reviews);
            result.add(response);
        }

        return result;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    public Map<String, Object> getProduct(@PathVariable("id") Long id)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();
        Product product = productRepository.findOne(id);

        List<Review> reviews = reviewRepository.findByProduct_ProductId(id);

        if (product != null)
        {
            response.put(PRODUCT, product);
            response.put(REVIEW, reviews);
        }
        return response;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> addProduct(String description, String productName, Float price)
    {
        Map<String, String> response = new LinkedHashMap<String, String>();
        Product product = new Product(productName, description, price);

        productRepository.save(product);

        response.put(PRODUCT, CREATE_SUCCESS);
        return response;
    }
}
