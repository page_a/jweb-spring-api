package eu.epitech.jweb.controllers;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.entity.Product;
import eu.epitech.jweb.entity.Review;
import eu.epitech.jweb.repository.IReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexa on 18/12/2015.
 */

/*
 * Controller for /review road.
 * RestController allowing to add remove review.
 * It is securised, only member can request with road. (@Secured)
 *
 * Road :
 *          /review -> POST -> Map<String, String> addReview(String comment, Long idProduct, Long authorId)
 *          /review/{id} -> DELETE -> Map<String, String> removeReview(Long id)
 *
 */

@RestController
@RequestMapping("/review")
@Secured({"ROLE_ADMIN", "ROLE_USER"})
public class ReviewController
{

    private static final String MESSAGE = "message";
    private static final String SUCCESS = "Review created successfully";
    private static final String DELETE_SUCCESS = "Review deleted successfully";

    @Autowired
    IReviewRepository IReviewRepository;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> addReview(String comment, Long idProduct, Long authorId)
    {
        Review rev = new Review(comment, new Product(idProduct), new Account(authorId), new Date());
        IReviewRepository.save(rev);

        Map<String, String> response = new HashMap<String, String>();
        response.put(MESSAGE, SUCCESS);
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseBody
    public Map<String, String> removeReview(@PathVariable("id") Long id)
    {
        IReviewRepository.delete(id);
        Map<String, String> response = new HashMap<String, String>();
        response.put(MESSAGE, DELETE_SUCCESS);
        return response;
    }
}
