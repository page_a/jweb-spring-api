package eu.epitech.jweb.controllers;

import eu.epitech.jweb.entity.Account;
import eu.epitech.jweb.exception.EmailInUseException;
import eu.epitech.jweb.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexa on 14/12/2015.
 */

/*
 * Controller for /account road.
 * RestController allowing to get all the member detail, create an User, get user detail, edit a User, or delete one.
 * It is securised, only admin can request with road. (@Secured)
 *
 * Road :
 *          /account/all -> GET -> getAllMember()
 *          /account -> POST -> createUser()
 *          /account/{accountId} -> GET -> getUserDetail()
 *          /account/{accountId} -> PUT -> editUser()
 *          /account/{accountId} -> DELETE -> deleteUser()
 *
 */
@RestController
@RequestMapping("/account")
@Secured({"ROLE_ADMIN"})
public class AccountController
{
    @Autowired
    private IAccountRepository IAccountRepository;

    private static final String CREATE_SUCCESS = "Account created successfully";
    private static final String UPDATE_SUCCESS = "Account successfully updated";
    private static final String DELETE_SUCCESS = "Account deleted successfully";
    private static final String EMAIL_IN_USE = "Email already in USE";
    private static final String DEFAULT_ROLE = "ROLE_USER";
    private static final String MESSAGE = "message";
    private static final String ACCOUNT = "account";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String NEWSLETTER_REGISTER = "newsletter";
    private static final String ROLE = "role";

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    @ResponseBody
    public List<Account> getMeAllTheseMember()
    {
        List<Account> members = IAccountRepository.findAll();

        return members;
    }

    @Secured({})
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createUser(String firstName, String lastName, String password,
                                          String email, Boolean newsletter)
    {
        Map<String, Object> response = new LinkedHashMap<String, Object>();

        Account acc = IAccountRepository.findFirstByEmail(email);
        System.out.println(firstName);
        System.out.println(lastName);
        System.out.println(password);
        System.out.println(email);
        System.out.println(newsletter);
        if (acc == null)
        {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String passwordHashed = passwordEncoder.encode(password);
            Account account = new Account(firstName,
                    lastName,
                    passwordHashed,
                    email,
                    DEFAULT_ROLE,
                    newsletter != null ? newsletter : true
                    );
            IAccountRepository.save(account);
            response.put(MESSAGE, CREATE_SUCCESS);
            response.put(ACCOUNT, account);
        } else
            throw new EmailInUseException(  );
        return response;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{accountId}")
    @ResponseBody
    public Account getUserDetails(@PathVariable("accountId") Long accountId)
    {
        return IAccountRepository.findOne(accountId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{accountId}")
    @ResponseBody
    public Map<String, Object> editUser(@PathVariable("accountId") Long accountId,
                                        @RequestBody Map<String, Object> accountMap)
    {
        Account account = new Account(accountMap.get(FIRST_NAME).toString(),
                accountMap.get(LAST_NAME).toString(),
                accountMap.get(PASSWORD).toString(),
                accountMap.get(EMAIL).toString(),
                accountMap.get(ROLE).toString(),
                Boolean.parseBoolean(accountMap.get(NEWSLETTER_REGISTER).toString())
        );
        account.setId(accountId);


        Map<String, Object> response = new LinkedHashMap<String, Object>();
        response.put(MESSAGE, UPDATE_SUCCESS);
        response.put(ACCOUNT, IAccountRepository.save(account));
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{accountId}")
    @ResponseBody
    public Map<String, String> deleteUser(@PathVariable("accountId") Long accountId)
    {

        IAccountRepository.delete(accountId);
        Map<String, String> response = new HashMap<String, String>();
        response.put(MESSAGE, DELETE_SUCCESS);
        return response;
    }
}
